from django.db import models
from django.utils import timezone
# Create your models here.


class Post(models.Model):
    author = models.ForeignKey('auth.User',on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date=models.DateTimeField(default=timezone.now)
    published_date=models.DateTimeField(blank=True,null=True)
    Age=models.IntegerField(blank=False,null=False)
    Email=models.EmailField(blank=False,null=False)
    G_C=(('M','Male'),('F','Female'))
    gender=models.CharField(max_length=1,choices=G_C,default='M')
    def published(self):
        self.published_date=timezone.now()
        self.save() 

    def __str__(self):
        return self.title   

    